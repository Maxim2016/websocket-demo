# WebSocket-Demo

Test project. Spring Boot with WebSocket as a server and React with SockJS as a client.

# Installing and running

First of all you will need to run a server. So, run an Application.java file using IDE


Then install and run JS client:

```shell
npm i
```

Run in dev mode

```shell
npm run start
```

Then open in your browser

```shell 
http://localhost:3001
```