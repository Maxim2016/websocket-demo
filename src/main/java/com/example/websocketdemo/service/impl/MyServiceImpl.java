package com.example.websocketdemo.service.impl;

import com.example.websocketdemo.dto.EventType;
import com.example.websocketdemo.dto.ObjectType;
import com.example.websocketdemo.model.Message;
import com.example.websocketdemo.service.MyService;
import com.example.websocketdemo.util.WsSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.function.BiConsumer;

@Service
public class MyServiceImpl implements MyService {

	private final BiConsumer<EventType, Message> wsSender;

	@Autowired
	public MyServiceImpl(WsSender wsSender) {
		this.wsSender = wsSender.getSender(ObjectType.MESSAGE);
	}

	@Override
	public void runProcessing() {

		try {
			for (int i=0; i<50; i++) {

				Message message = new Message();
				message.setId((long) i);
				message.setText("Test text " + i);

				wsSender.accept(EventType.CREATE, message);

				Thread.sleep(5000);
			}
		} catch (InterruptedException ie) {
			Thread.currentThread().interrupt();
		}
	}
}
