package com.example.websocketdemo.dto;

public enum EventType {
	CREATE, UPDATE, REMOVE
}
