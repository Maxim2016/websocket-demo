package com.example.websocketdemo.controller;

import com.example.websocketdemo.service.MyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.TaskExecutor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MessageController {

	@Qualifier(value = "threadPoolTaskExecutor")
	@Autowired
	private TaskExecutor taskExecutor;

	@Autowired
	private MyService myService;

	@GetMapping("/start")
	public void start() {
		taskExecutor.execute(() -> myService.runProcessing());
	}
}
