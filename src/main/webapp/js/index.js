import React from "react";
import ReactDom from "react-dom";
import SockJsClient from "react-stomp";
import Fetch from "json-fetch";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      clientConnected: false,
      messages: []
    };
  }

  onMessageReceive = (msg, topic) => {
    console.log(topic, msg);
    this.setState({ messages: [...this.state.messages, msg] });
  };

  handleClick = () => {
    Fetch('http://localhost:8080/start', {
      method: 'GET',
      mode: 'no-cors',
      credentials: 'include',
    });
  };

  render() {
    const wsSourceUrl = "http://localhost:8080/handler";
    return (
      <div>
        <SockJsClient url={ wsSourceUrl } topics={["/topic/all"]}
                      onMessage={ this.onMessageReceive }
                      onConnect={ () => {
                        console.log('Connected');
                        this.setState({ clientConnected: true })
                      } }
                      onDisconnect={ () => { this.setState({ clientConnected: false }) } }
                      debug={ false }/>

        <span>Client connected: { this.state.clientConnected ? 'Yes' : 'No' }</span>

        {
          this.state.clientConnected && (
            <div>
              <button className="btn btn-default" onClick={ this.handleClick }>Start</button>
            </div>
          )
        }

        {
          this.state.messages.length > 0 && this.state.messages.map((m, index) => {
            return <div key={ `message-${index}` }>{ `Object Type: ${ m.objectType } Text:${ m.body.text }` }</div>
          })
        }

      </div>
    );
  }
}

ReactDom.render(<App />, document.getElementById("root"));
