const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');


module.exports = {
  entry: './src/main/webapp/js/index.js',

  devtool: 'inline-source-map',

  devServer: {
    contentBase: './dist',
    port: 3001,
  },

  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  module: {
    loaders: [
      {
        test: /\.(js|jsx)$/,
        include: path.resolve(__dirname, 'src', 'main', 'webapp', 'js'),
        loader: "babel-loader",
        query: {
          presets: ["env", "react", "stage-2"]
        }
      }
    ]
  },

  plugins: [
    new HtmlWebpackPlugin({
      title: 'Spring WebSockets with ReactJs',
      template: './src/main/resources/templates/index.html',
    })
  ],

};
